import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { Paciente } from '../model/paciente.model';
import {GenericService} from './generic.service'
@Injectable({
  providedIn: 'root'
})
export class PacienteService extends GenericService<Paciente, Number>{

  paciente = `/paciente`
  uploadImageData = new FormData()
  constructor(private http : HttpClient) {
    super(http, 'paciente')
    
   }

   uploadImage(id:number,value:any){
    var formData = new FormData();

    formData.append('image', value, value.name);
     return this.http.post('http://localhost:8080/image/upload/'+id, formData, { observe: 'response' })
   }
}
