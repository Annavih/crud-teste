import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GenericService<T, ID> {

  api = "http://localhost:8080"

  constructor(private _http: HttpClient,
    private _endpoint: string) { }

  findOne(id: ID): Observable<T> {
    return this._http.get<T>(`${this.api}/${this._endpoint}/${id}`).pipe(
      map(this.jsonDataToEntity)
    );
  }

  list(): Observable<T[]> {
    return this._http.get<T[]>(`${this.api}/${this._endpoint}`).pipe(
      map(this.jsonDataToEntities)
    );
  }

  findAll(queryString?: string): Observable<T[]> {
    return this._http.get<T[]>(`${this.api}/${this._endpoint}?${queryString}`).pipe(
      map(this.jsonDataToEntities)
    );
  }

  save(entity: T): Observable<T> {
    return this._http.post<T>(`${this.api}/${this._endpoint}`, entity).pipe(
      map(this.jsonDataToEntity)
    );
  }

  update(id: ID, entity: T): Observable<T> {
    return this._http.put<T>(`${this.api}/${this._endpoint}/${id}`, entity).pipe(
      map(this.jsonDataToEntity)
    );
  }

  deleteOne(id: ID): Observable<T> {
    return this._http.delete<T>(`${this.api}/${this._endpoint}/${id}`).pipe(
      map(() => null)
    );
  }

  delete(entity: T): Observable<T> {
    return this._http.delete<T>(`${this.api}/${this._endpoint}`, entity).pipe(
      map(() => null)
    );
  }

  // PRIVATE METHODS

  jsonDataToEntities(jsonData: any[]): T[] {
    const objects: T[] = [];
    jsonData.forEach(element => objects.push(element as T));
    return objects;
  }

  jsonDataToEntity(jsonData: any): T {
    return jsonData as T;
  }

}