import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public isLoggedIn = false;

  form: FormGroup

  constructor(private formBuild : FormBuilder, private serviceUser: UserService) { }

  ngOnInit(): void {
    this.setForm()
  }

  setForm(){
    this.form = this.formBuild.group({
      login: new FormControl('', Validators.required),
      senha: new FormControl('', Validators.required)
    })
  }

}
