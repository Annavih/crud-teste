export class Endereco{
     id: number;
	 logradouro: string;
	 uf: string;
	 municipio: string;
	 numero: string;
	 bairro: string;
	 complemento: string;
	 cep: string;
	 
}