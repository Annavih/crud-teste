import { Endereco } from "./endereco.model";
import { Telefone } from "./telefone.model";

export class Paciente {
     id :number;
	 nome:string;
	 cpf: string;
	 rg: string;
	 nomeMae: string;
	 nomePai: string;
	 dataNascimento:String;
	 sexo:string;
	 telefones:Array<Telefone>;
	 urlFoto:string;
	 endereco: Endereco;
}