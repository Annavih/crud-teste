import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PacienteDetalhesComponent } from './paciente-detalhes/paciente-detalhes.component';
import { PacienteListaComponent } from './paciente-lista/paciente-lista.component';
import { PacienteNovoComponent } from './paciente-novo/paciente-novo.component';

const routes: Routes = [
  {path:"lista", component:PacienteListaComponent, pathMatch:'full'},
  {path:"novo", component:PacienteNovoComponent},
  {path:"detalhes/:id", component:PacienteDetalhesComponent},
  {path:"editar/:id", component:PacienteNovoComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacienteRoutingModule { }
