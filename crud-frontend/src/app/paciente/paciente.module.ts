import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PacienteRoutingModule } from './paciente-routing.module';
import { PacienteNovoComponent } from './paciente-novo/paciente-novo.component';
import { PacienteListaComponent } from './paciente-lista/paciente-lista.component';
import {InputTextModule} from 'primeng/inputtext';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {DropdownModule} from 'primeng/dropdown';
import {FileUploadModule} from 'primeng/fileupload';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {InputMaskModule} from 'primeng/inputmask';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {TableModule} from 'primeng/table';
import { PacienteDetalhesComponent } from './paciente-detalhes/paciente-detalhes.component';
import {CalendarModule} from 'primeng/calendar';


@NgModule({
  declarations: [PacienteNovoComponent, PacienteListaComponent, PacienteDetalhesComponent],
  imports: [
    CommonModule,
    PacienteRoutingModule,
    InputTextModule,
    CardModule,
    ButtonModule,
    DropdownModule,
    FileUploadModule,
    HttpClientModule,
    FormsModule,
    InputMaskModule,
    BreadcrumbModule,  
    TableModule,
    CalendarModule
  ],
  exports:[]
})
export class PacienteModule { }
