import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Paciente } from 'src/app/model/paciente.model';
import { Telefone } from 'src/app/model/telefone.model';
import { PacienteService } from 'src/app/service/paciente.service';

@Component({
  selector: 'app-paciente-detalhes',
  templateUrl: './paciente-detalhes.component.html',
  styleUrls: ['./paciente-detalhes.component.scss']
})
export class PacienteDetalhesComponent implements OnInit {
  items = []
  paciente: Paciente;
  telefones: Array<Telefone>
  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
  uploadForm: FormGroup;  
  urlImagem: string
  constructor(private pacienteService: PacienteService,
    private router: ActivatedRoute,
    private route: Router) { }

  ngOnInit(): void {
    this.items = [
      { label: 'Lista de Pacientes', routerLink: '/paciente/lista' },
      { label: 'Detalhes do Paciente' },
    ];

    this.pacienteService.findOne(this.router.snapshot.params['id']).subscribe(data => {
      this.paciente = data;
      this.telefones = data.telefones;
    })
    
  }

  editar() {
    this.route.navigate([`/paciente/editar/${this.router.snapshot.params['id']}`])
  }

  deletar() {
    this.pacienteService.deleteOne(this.router.snapshot.params['id']).subscribe(data => {
      this.route.navigate([`/paciente/lista`])
    })
  }

  public onFileChanged(event) {
    if (event.target.files && event.target.files.length > 0) {
      var reader = new FileReader()

      reader.onload = (event :any ) =>{
        this.urlImagem = event.target.result
      }
      reader.readAsDataURL(event.target.files[0]);
      this.selectedFile = event.target.files[0];
      console.log(this.selectedFile)
    }
  }

  onBasicUpload = () => {
  
    this.pacienteService.uploadImage(this.router.snapshot.params['id'], this.selectedFile).subscribe((response) => {
      if (response.status === 200) {
        this.message = 'Image uploaded successfully';
      } else {
        this.message = 'Image not uploaded successfully';
      }
    });
  }
}
