import { Component, OnInit, ɵConsole } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Endereco } from 'src/app/model/endereco.model';
import { Paciente } from 'src/app/model/paciente.model';
import { Telefone } from 'src/app/model/telefone.model';
import { PacienteService } from 'src/app/service/paciente.service';

@Component({
  selector: 'app-paciente-novo',
  templateUrl: './paciente-novo.component.html',
  styleUrls: ['./paciente-novo.component.scss']
})
export class PacienteNovoComponent implements OnInit {
  sexos = []

  paciente = new Paciente();
  endereco = new Endereco()
  telefones: Array<Telefone> = [{ id: null, fone: '' }]
  items = [];
  br: any
  dataNascimento: Date
  update: boolean = false
  constructor(private pacienteService: PacienteService, private router: ActivatedRoute, private route : Router) { }

  ngOnInit(): void {
    this.items = [
      { label: 'Novo Paciente' },
    ];

    this.sexos = [
      { label: 'Masculino', value: 'M' },
      { label: 'Femenino', value: 'F' },
    ];

    this.br = {
      firstDayOfWeek: 0,
      dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
      dayNamesShort: ["Don", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
      dayNamesMin: ["Don", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
      monthNames: ["Janeiro", "Feveiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
      today: 'Today',
      clear: 'Clear',
      dateFormat: 'dd/mm/yyyy',
      weekHeader: 'Wk'
    };

    if (this.router.snapshot.params['id'] !== undefined) {
      this.update = true;

      this.pacienteService.findOne(this.router.snapshot.params['id']).subscribe(data => {
        this.paciente = data;
        this.endereco = data.endereco;
        this.telefones = data.telefones;
      })
    }
  }


  myUploader(event) {
    console.log(event)
  }

  save() {
    let data = this.paciente;
    data.dataNascimento = new Date(this.dataNascimento).toLocaleDateString()
    data.endereco = this.endereco;
    data.telefones = this.telefones;
    if (this.update) {
      this.pacienteService.update(data.id, data).subscribe(data => {
        if (data) {
          this.route.navigate([`/paciente/lista`])
        }
      })
    } else {
      this.pacienteService.save(data).subscribe(data => {
          this.route.navigate([`/paciente/lista`])
        
      })
    }
  }

  addFone() {
    this.telefones.push({ id: null, fone: '' })
  }

}
