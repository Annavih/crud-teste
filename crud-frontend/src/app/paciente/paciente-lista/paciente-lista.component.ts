import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PacienteService } from 'src/app/service/paciente.service';
import {Paciente} from '../../model/paciente.model'
@Component({
  selector: 'app-paciente-lista',
  templateUrl: './paciente-lista.component.html',
  styleUrls: ['./paciente-lista.component.scss']
})
export class PacienteListaComponent implements OnInit {
  items = []
  pacientes : Paciente[];
  constructor(private pacienteService: PacienteService, private router : Router) { }

  ngOnInit(): void {
    this.items = [
      { label: 'Lista de Pacientes' },
    ];

    this.pacienteService.list().subscribe( data => {
      this.pacientes = data;
    })

  }

  detalhes(id){
    this.router.navigate([`/paciente/detalhes/${id}`])
  }

  deletar(id){
    this.pacienteService.deleteOne(id).subscribe(data => {
      this.pacienteService.list().subscribe( data => {
        this.pacientes = data;
      })
    })
  }

}
