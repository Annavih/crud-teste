import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: '', component: AppComponent, pathMatch: "full" },
  { path: 'login', component: LoginComponent },
  { path: 'paciente', loadChildren: () => import('./paciente/paciente.module').then(m => m.PacienteModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
