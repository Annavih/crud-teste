import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  items = []
  show: boolean = true;
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        if (evt.url === '/login') {
          this.show = false
        }
      }
    })
    this.items = [
      {
        label: 'Crud Frontend',
      },
      {
        label: 'Novo Paciente',
        icon: 'pi pi-fw pi-pencil',
        routerLink: '/paciente/novo'
      },
      {
        label: 'Pacientes',
        icon: 'pi pi-users',
        routerLink: '/paciente/lista'
      },
      {
        label: 'Sair',
        icon: 'pi pi-sign-in',
        routerLink: '/login'
      }
    ];
  }

}
