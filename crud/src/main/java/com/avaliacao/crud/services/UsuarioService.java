package com.avaliacao.crud.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avaliacao.crud.model.Usuario;
import com.avaliacao.crud.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	UsuarioRepository repository;
	
	public List<Usuario> findAll(){
		return repository.findAll();
	}
	
	public Usuario save(Usuario usuario) {
		return repository.save(usuario);
	}
	
	public Optional<Usuario> findOne(Long id) {
		return repository.findById(id);
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}
}
