package com.avaliacao.crud.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avaliacao.crud.model.Paciente;

import com.avaliacao.crud.repository.PacienteReposity;

@Service
public class PacienteSevice {

	@Autowired
	PacienteReposity pacienteReposity;

	public void Save(Paciente paciente) {
		pacienteReposity.save(paciente);
	}
	
	public void Update(Paciente paciente) {
		pacienteReposity.save(paciente);
	}

	public List<Paciente> findAll() {
		return pacienteReposity.findAll();
	}
	
	public Optional<Paciente> findById(Long id) {
		return pacienteReposity.findById(id);
	}
	
	
	public void delete(Long id) {
		pacienteReposity.deleteById(id);
	}

}
