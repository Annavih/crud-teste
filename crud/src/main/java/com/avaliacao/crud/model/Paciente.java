package com.avaliacao.crud.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "paciente", schema = "public")
public class Paciente implements Serializable  {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private String nome;
	private String cpf;
	private String rg;
	private String nomeMae;
	private String nomePai;
	private String dataNascimento;
	private Character sexo;
	@OneToMany(cascade = {CascadeType.ALL})
	private List<Telefone> telefones;
	@OneToOne(cascade = {CascadeType.ALL})
	private Endereco endereco;
	

	public Paciente() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Paciente(Long id, String nome, String cpf, String rg, String nomeMae, String nomePai, String dataNascimento,
			Character sexo, List<Telefone> telefones, String urlFoto, Endereco endereco) {
		super();
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.rg = rg;
		this.nomeMae = nomeMae;
		this.nomePai = nomePai;
		this.dataNascimento = dataNascimento;
		this.sexo = sexo;
		this.telefones = telefones;
		this.endereco = endereco;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getNomeMae() {
		return nomeMae;
	}
	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}
	public String getNomePai() {
		return nomePai;
	}
	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}
	public String getIdade() {
		return dataNascimento;
	}
	public void setIdade(String idade) {
		this.dataNascimento = idade;
	}
	public Character getSexo() {
		return sexo;
	}
	public void setSexo(Character sexo) {
		this.sexo = sexo;
	}
	public List<Telefone> getTelefones() {
		return telefones;
	}
	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		

	
	
}
