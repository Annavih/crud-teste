package com.avaliacao.crud.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avaliacao.crud.model.Paciente;

import com.avaliacao.crud.services.PacienteSevice;;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value="/paciente")
public class PacienteController {
	@Autowired
	PacienteSevice pacienteSevice;

	@PostMapping(value = "")
	public void Save(@RequestBody Paciente paciente) {
		pacienteSevice.Save(paciente);
	}
	
	@PutMapping(value = "")
	public void Update(Paciente paciente) {
		pacienteSevice.Update(paciente);
	}
	
	@DeleteMapping(value = "/{id}")
	public void Delete(@PathVariable Long id) {
		pacienteSevice.delete(id);
	}

	@GetMapping(value = "")
	public ResponseEntity<?> findAll() {
		List<Paciente> pacientes = pacienteSevice.findAll();
		return ResponseEntity.ok().body(pacientes);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<?> findById(@PathVariable(name = "id") Long id) {
		Optional<Paciente> paciente = pacienteSevice.findById(id);
		return ResponseEntity.ok().body(paciente);
	}

}
