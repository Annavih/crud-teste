package com.avaliacao.crud.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.avaliacao.crud.model.Image;

public interface ImageRepository extends JpaRepository<Image, Long> {

	Optional<Image> findByPacienteId(Long pacienteId);
}
