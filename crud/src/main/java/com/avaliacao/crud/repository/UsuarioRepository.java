package com.avaliacao.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avaliacao.crud.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{

	Usuario findByLogin(String login);
	
}
