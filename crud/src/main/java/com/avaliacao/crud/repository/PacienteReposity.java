package com.avaliacao.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avaliacao.crud.model.Paciente;

public interface PacienteReposity extends JpaRepository<Paciente, Long> {


}
